import sys
import requests
import csv
import json
from PyQt5 import QtWidgets
import test_ui


class TestApp(QtWidgets.QMainWindow, test_ui.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.csv_button.clicked.connect(self.use_csv)
        self.json_button.clicked.connect(self.use_json)

    def use_csv(self):
        csv_contents = requests.get("http://ir.eia.gov/ngs/wngsr.csv").text
        print(csv_contents)
        reader = csv.reader(csv_contents.splitlines())
        avg = 0
        nums = 0
        for row_num, row in enumerate(reader):
            print("|".join(row))
            for col_num, col in enumerate(row):
                if col_num == 7:
                    if 7 <= row_num <= 13:
                        avg += abs(int(col))
                        nums += 1
                    if row_num == 14:
                        print("Total: " + col)
                        self.total.setText(col)
        avg /= nums
        print("Avg: " + str(avg))
        self.avg.setText(str(round(avg, 2)))

    def use_json(self):
        json_contents = requests.get("http://ir.eia.gov/ngs/wngsr.json").content
        print(json_contents)
        json_d = json.loads(json_contents)
        series = json_d["series"]
        avg = 0
        nums = 0
        for region in series:
            net_change = region["calculated"]["net_change"]
            if region["name"] == "total lower 48 states":
                print("Total: " + str(net_change))
                self.total.setText(str(net_change))
            else:
                avg += abs(net_change)
                nums += 1

        if nums > 0:
            avg /= nums
        print("Avg: " + str(avg))
        self.avg.setText(str(round(avg, 2)))


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = TestApp()
    window.show()
    app.exec_()


if __name__ == '__main__':
    main()
