# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'test_ui.ui'
#
# Created by: PyQt5 UI code generator 5.13.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(320, 240)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.csv_button = QtWidgets.QPushButton(self.centralwidget)
        self.csv_button.setGeometry(QtCore.QRect(20, 180, 88, 34))
        self.csv_button.setObjectName("csv_button")
        self.json_button = QtWidgets.QPushButton(self.centralwidget)
        self.json_button.setGeometry(QtCore.QRect(210, 180, 88, 34))
        self.json_button.setObjectName("json_button")
        self.total_label = QtWidgets.QLabel(self.centralwidget)
        self.total_label.setGeometry(QtCore.QRect(50, 20, 58, 18))
        self.total_label.setAlignment(QtCore.Qt.AlignCenter)
        self.total_label.setObjectName("total_label")
        self.avg_label = QtWidgets.QLabel(self.centralwidget)
        self.avg_label.setGeometry(QtCore.QRect(200, 20, 58, 18))
        self.avg_label.setAlignment(QtCore.Qt.AlignCenter)
        self.avg_label.setObjectName("avg_label")
        self.total = QtWidgets.QLabel(self.centralwidget)
        self.total.setGeometry(QtCore.QRect(50, 50, 58, 18))
        self.total.setText("")
        self.total.setAlignment(QtCore.Qt.AlignCenter)
        self.total.setObjectName("total")
        self.avg = QtWidgets.QLabel(self.centralwidget)
        self.avg.setGeometry(QtCore.QRect(200, 50, 58, 18))
        self.avg.setText("")
        self.avg.setAlignment(QtCore.Qt.AlignCenter)
        self.avg.setObjectName("avg")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.csv_button.setText(_translate("MainWindow", "CSV"))
        self.json_button.setText(_translate("MainWindow", "JSON"))
        self.total_label.setText(_translate("MainWindow", "Total"))
        self.avg_label.setText(_translate("MainWindow", "Avg"))
